package com.hillel;

public class Thing implements Comparable<Thing> {
    String name;
    int price;
    int weight;

    public Thing(String name, int price, int weight) {
        this.name = name;
        this.price = price;
        this.weight = weight;
    }

    @Override
    public int compareTo(Thing o) {
        if (this.name.equals(o.name)) {
            if (this.price == o.price) {
                return this.weight - o.weight;
            }
            else {
                return (this.price - o.price);
            }
        }
        else {
            return (this.name.compareTo(o.name));
        }
    }
}
