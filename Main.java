package com.hillel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> listOfNames = List.of("bat", "ball", "glove", "glove", "glove", "bat");
        List<Integer> listOfPrices = List.of(2, 3, 1, 1, 1, 2);
        List<Integer> listOfWeights = List.of(2, 5, 1, 1, 1, 1);
        System.out.println(numDuplicates(listOfNames, listOfPrices, listOfWeights));
    }

    public static int numDuplicates(List<String> names, List<Integer> prices, List<Integer> weights) {
        int count = 0;
        List<Thing> listOfThings = new ArrayList<>();
        for (int i = 0; i < names.size(); ++i) {
            Thing thing = new Thing (names.get(i), prices.get(i), weights.get(i));
            listOfThings.add(thing);
        }
        listOfThings.sort(Thing::compareTo);
        for (int i = 0; i < listOfThings.size(); ++i) {
            for (int j = i + 1; j < listOfThings.size(); ++j) {
                if (listOfThings.get(i).name.equals(listOfThings.get(j).name) &&
                        listOfThings.get(i).price == listOfThings.get(j).price &&
                        listOfThings.get(i).weight == listOfThings.get(j).weight) {
                    ++count;
                    listOfThings.remove(listOfThings.get(j));
                    --j;
                }
            }
        }
        return count;
    }
}
